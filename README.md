# Substrate Calculator - Discord Bot

![output](img/output.png)

[[_TOC_]]

## Usage

Usually you can start typing a forward slash `/` to bring up the command menu in your discord client. From there, you can either select the command you want or finish typing the command yourself.

![slash menu](img/slash_selection.png)

There are two commands, one for imperial and the other for metric calculations.

- `/substrate_calculator_imperial`
- `/substrate_calculator_metric`

The only argument which is required is the tank size variable `tank_selection`. Depending on the units you selected, commonly found tanks will show up in a selection box. If you would like to specify the dimensions of your tank, select `Custom` at the end of the list and enter the `length`, `width`, and `height` of your tank.

![input](img/input.png)

You can also specify the `dirt` and `sand` layer depths.

## Reporting issues

Please use the issue tracker on this repository for reporting issues.

https://gitlab.com/neva/substrate-calculator/-/issues/new

## Testing

You can test it out here in the discord server I was using for testing:

https://discord.gg/jQ8qZvwY

## Invite to your own server

Invite link to your server:

https://discord.com/api/oauth2/authorize?client_id=1205204456753332255&permissions=2048&scope=bot

It has two permissions to grant: to be a `bot` and permission to `send text messages` to the channel. No other privs required.
