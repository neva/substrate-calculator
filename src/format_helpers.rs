/// Rounds a floating-point number to the nearest fraction specified by
/// `rounding_factor`.
///
/// # Arguments
///
/// * `value` - The floating-point number to be rounded.
/// * `rounding_factor` - The denominator of the fraction to round to. For
///   example, use 2 to round to the nearest half, 4 for the nearest quarter, 8
///   for the nearest eighth, etc. This is an integer value.
///
/// # Returns
///
/// The rounded value as a floating-point number.
///
/// # Examples
///
/// ```
/// // Rounds to 3.5 (nearest half)
/// let rounded_half = round_to_nearest_fraction(3.25, 2); 
/// // Rounds to 3.25 (nearest quarter)
/// let rounded_quarter = round_to_nearest_fraction(3.12, 4); 
/// // Rounds to 3.125 (nearest eighth)
/// let rounded_eighth = round_to_nearest_fraction(3.12, 8); 
/// ```
pub fn round_to_nearest_fraction(value: f64, rounding_factor: i8) -> f64 {
    round(value, rounding_factor as f64)
}

pub fn round(value: f64, rounding_factor: f64) -> f64 {
    (value * rounding_factor).ceil() / rounding_factor
}