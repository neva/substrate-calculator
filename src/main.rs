mod tank;
mod substrate;
mod format_helpers;

use poise::serenity_prelude as serenity;
use tank::{Tank, TankImperial, TankMetric};
use substrate::SubstrateResult;
use format_helpers::round_to_nearest_fraction;

struct Data {} // User data, which is stored and accessible in all command invocations
type Error = Box<dyn std::error::Error + Send + Sync>;
type Context<'a> = poise::Context<'a, Data, Error>;

/// Calculates the volumes for a "Father Fish" dirted deep substrate system
///
/// This command allows users to select a predefined tank size or input custom
/// dimensions. It then computes the necessary volumes of substrate components
/// (sphagnum moss, soil, compost, supplement) and the weight of the sand cap
/// needed to achieve the desired substrate system setup.
///
/// # Parameters
/// - `ctx`: The context in which this command is executed, containing
///   information like the channel and user.
/// - `tank_selection`: Enum representing the predefined tank sizes or a custom
///   option. Custom tank sizes require additional dimensions (length, width,
///   height).
/// - `l`: Optional. The length of the tank in inches. Required if
///   `tank_selection` is `TankCustom`.
/// - `w`: Optional. The width of the tank in inches. Required if
///   `tank_selection` is `TankCustom`.
/// - `h`: Optional. The height of the tank in inches. Required if
///   `tank_selection` is `TankCustom`.
///
/// # Returns
/// This function does not return a value but sends a message to the invoking
/// context with the calculated substrate system setup.
///
/// # Errors
/// Returns an error if there is an issue sending the response message to the
/// context.
///
/// # Example
/// `/substrate_calculator_imperial tank_selection:Custom l:48 w:12 h:24` -
/// Calculates and displays the substrate volumes for a custom tank size of
/// 48x12x24 inches.
#[poise::command(slash_command)]
async fn substrate_calculator_imperial(
    ctx: Context<'_>,
    #[description = "Tank Size"] tank_selection: TankImperial,
    #[min = 8_f64]
    #[max = 240_f64]
    #[description = "Length (inches)"] length: Option<f64>,
    #[min = 8_f64]
    #[max = 240_f64]
    #[description = "Width (inches)"] width: Option<f64>,
    #[min = 8_f64]
    #[max = 240_f64]
    #[description = "Height (inches)"] height: Option<f64>,
    #[min = 0_f64]
    #[max = 10.0_f64]
    #[description = "Dirt (inches)"] dirt: Option<f64>,
    #[min = 0_f64]
    #[max = 10.0_f64]
    #[description = "Sand (inches)"] sand: Option<f64>,
) -> Result<(), Error> {
    let l: f64 = length.unwrap_or(0.0);
    let h: f64 = height.unwrap_or(0.0);
    let w: f64 = width.unwrap_or(0.0);
    let dirt: f64 = dirt.unwrap_or(1.0);
    let sand: f64 = sand.unwrap_or(2.0);

    let tank = match tank_selection {
        TankImperial::TankCustom => {
            Tank {
                description: format!(
                    "Custom {:2} gallon ({}\" x {}\" x {}\")",
                    round_to_nearest_fraction((l * w * h) / 231.0, 4), l, w, h,
                ),
                length: l,
                width: w,
                height: h,
            }
        },
        _ => tank_selection.to_tank(),
    };
    let substrate = SubstrateResult::new(
        tank.length.into(), 
        tank.height.into(), 
        tank.width.into(),
        dirt.into(),
        sand.into(),
    );

    ctx.say(substrate.format_summary_imperial(tank.description)).await?;
    Ok(())
}

/// Calculates the volumes for a "Father Fish" dirted deep substrate system
///
/// This command allows users to select a predefined tank size or input custom
/// dimensions in metric units. It computes the necessary volumes of substrate
/// components (sphagnum moss, soil, compost, supplement) and the weight of the
/// sand cap needed to achieve the desired substrate system setup, converting
/// all measurements into metric units.
///
/// # Parameters
/// - `ctx`: The context in which this command is executed, containing
///   information like the channel and user.
/// - `tank_selection`: Enum representing predefined tank sizes in metric units
///   or a custom option. Custom tank sizes require additional dimensions
///   (length, width, height) in centimeters.
/// - `l`: Optional. The length of the tank in centimeters. Required if
///   `tank_selection` is `TankCustom`.
/// - `w`: Optional. The width of the tank in centimeters. Required if
///   `tank_selection` is `TankCustom`.
/// - `h`: Optional. The height of the tank in centimeters. Required if
///   `tank_selection` is `TankCustom`.
///
/// # Returns
/// Does not return a value but sends a message to the invoking context with the
/// calculated substrate system setup in metric units.
///
/// # Errors
/// Returns an error if there is an issue sending the response message to the
/// context.
///
/// # Example
/// `/substrate_calculator_metric tank_selection:Custom l:120 w:30 h:60` -
/// Calculates and displays the substrate volumes for a custom tank size of
/// 120x30x60 cm.
#[poise::command(slash_command)]
async fn substrate_calculator_metric(
    ctx: Context<'_>,
    #[description = "Tank Size"] tank_selection: TankMetric,
    #[min = 18.0_f64]
    #[max = 600.0_f64]
    #[description = "Length (cm)"] length: Option<f64>,
    #[min = 18.0_f64]
    #[max = 600.0_f64]
    #[description = "Width (cm)"] width: Option<f64>,
    #[min = 18.0_f64]
    #[max = 600.0_f64]
    #[description = "Height (cm)"] height: Option<f64>,
    #[min = 0_f64]
    #[max = 25.0_f64]
    #[description = "Dirt (cm)"] dirt: Option<f64>,
    #[min = 0_f64]
    #[max = 25.0_f64]
    #[description = "Sand (cm)"] sand: Option<f64>,
) -> Result<(), Error> {
    let l: f64 = length.unwrap_or(0.0);
    let h: f64 = height.unwrap_or(0.0);
    let w: f64 = width.unwrap_or(0.0);
    let dirt: f64 = dirt.unwrap_or(2.5);
    let sand: f64 = sand.unwrap_or(5.0);

    let tank = match tank_selection {
        TankMetric::TankCustom => {
            Tank {
                description: format!(
                    "Custom ({}cm x {}cm x {}cm)",
                    l, w, h,
                ),
                length: l,
                width: w,
                height: h,
            }
        },
        _ => tank_selection.to_tank(),
    };

    let substrate = SubstrateResult::new_metric(
        tank.length.into(), 
        tank.height.into(), 
        tank.width.into(),
        dirt.into(),
        sand.into(),
    );
    
    ctx.say(substrate.format_summary_metric(tank.description)).await?;
    Ok(())
}

#[tokio::main]
async fn main() {
    let token = std::env::var("DISCORD_TOKEN").expect("missing DISCORD_TOKEN");
    let intents = serenity::GatewayIntents::non_privileged();

    let framework = poise::Framework::builder()
        .options(poise::FrameworkOptions {
            commands: vec![substrate_calculator_imperial(), substrate_calculator_metric()],
            ..Default::default()
        })
        .setup(|ctx, _ready, framework| {
            Box::pin(async move {
                poise::builtins::register_globally(ctx, &framework.options().commands).await?;
                Ok(Data {})
            })
        })
        .build();

    let client = serenity::ClientBuilder::new(token, intents)
        .framework(framework)
        .await;
    client.unwrap().start().await.unwrap();
}
