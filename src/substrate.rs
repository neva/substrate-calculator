use crate::format_helpers::{round_to_nearest_fraction, round};

/// This line sets a multiplier to account for the volume reduction when dry
/// dirt becomes wet. It's assumed that wet dirt occupies 80% of the volume of
/// dry dirt due to water filling in the gaps between particles.
const DIRT_TO_WATER_MULTIPLIER: f64 = 0.8;

/// Similar to the dirt multiplier, this one is used for converting the volume
/// of sand (in cups) to its weight. It implies that the weight of sand is 80%
/// of its volume in cups, likely considering a specific density or packing
/// factor.
const CUPS_TO_SAND_WEIGHT_MULTIPLIER: f64 = 0.8;

/// This represents the conversion factor from cubic inches to cups. 1 cubic
/// inch is equivalent to approximately 0.0692641 US cups. This factor is used
/// to convert substrate volume from cups to cubic inches.
const CUBIC_INCHES_TO_CUPS: f64 = 0.069;

/// Father Fish ratio of moss in dirt substrate (1/2th of mixture)
const RATIO_MOSS: f64 = 0.5;

/// Father Fish ratio of unfertalized soil in dirt substrate (1/4th of mixture)
const RATIO_SOIL: f64 = 0.25;

/// Father Fish ratio of compost in dirt substrate (1/4th of mixture)
const RATIO_COMPOST: f64 = 0.25;

/// Father Fish max ratio of supplement in dirt substrate (1/16th of mixture)
const RATIO_SUPPLEMENT_MAX: f64 = 0.0625;

/// Father Fish min ratio of supplement in dirt substrate (1/32nd of mixture)
const RATIO_SUPPLEMENT_MIN: f64 = 0.03125;

/// Ratio of cubic inches to gallons (1/231)
const CUBIC_INCHES_TO_GALLONS: f64 = 0.0043;

/// Amount from top of tank volume in inches water will not fill 
const TOP_OF_TANK_EMPTY_SPACE: f64 = 1.0;

/// Convert centimeters to inches
const CENTIMETERS_TO_INCHES: f64 = 2.54;

/// Convert cups to ml
const CUPS_TO_ML: f64 = 236.588;

/// Convert lb to kg
const LB_TO_KG: f64 = 0.453592;

/// Convert gal to liter
const GAL_TO_LITER: f64 = 3.78541;

/// Convert ml to l
const ML_TO_L: f64 = 1000.0;

#[derive(Debug, Clone)]
pub struct SubstrateResult {
    height_dirt: f64,
    height_sand: f64,
    cups_dirt_wet: f64,
    cups_dirt_dry_display: f64,
    cups_dirt_water: f64,
    cups_moss: f64,
    cups_soil: f64,
    cups_compost: f64,
    cups_supplement_max: f64,
    cups_supplement_min: f64,
    cups_sand: f64,
    weight_sand: f64,
    water_volume_gallons: f64,
}

impl SubstrateResult {
    /// Constructs a new `SubstrateResult` instance based on the specified
    /// dimensions and substrate layer heights.
    ///
    /// This method calculates the volumes of various substrate components
    /// (sphagnum moss, unfertilized soil, compost, and supplements) and the
    /// water needed to saturate the dirt layer. It also computes the volume of
    /// sand required to achieve a specific layer height and the volume of water
    /// that can be added to the tank, considering the space occupied by the
    /// substrate and a designated empty space at the top of the tank.
    ///
    /// # Arguments
    /// - `length`: The length of the fish tank in inches.
    /// - `height`: The total height of the fish tank in inches.
    /// - `width`: The width of the fish tank in inches.
    /// - `height_dirt`: The desired height of the dirt substrate layer in
    ///   inches.
    /// - `height_sand`: The desired height of the sand cap layer in inches.
    ///
    /// # Calculations
    /// - Converts the volume of dirt (including water saturation) from cubic
    ///   inches to cups, rounded to the nearest half cup, to estimate the wet
    ///   dirt volume.
    /// - Estimates the dry dirt volume as 80% of the wet volume to account for
    ///   the space taken up by water, rounding to the nearest half cup.
    /// - Distributes the dry dirt volume among moss, soil, and compost based on
    ///   predefined ratios, ignoring supplements in the total composition
    ///   calculation.
    /// - Calculates the minimum and maximum volumes of supplements, rounded to
    ///   the nearest eighth of a cup, as a proportion of the dry dirt volume.
    /// - Calculates the volume of water needed to saturate the dirt by
    ///   subtracting the dry components from the total wet volume.
    /// - Provides the total volume of the dirt mixture (dry dirt plus maximum
    ///   supplement volume) to be added to the tank to reach a 1-inch layer
    ///   height.
    /// - Calculates the total volume of sand needed to achieve a 2-inch layer
    ///   height in the tank, in cups.
    /// - Estimates the weight of the sand required in pounds, rounding up to
    ///   the nearest whole number.
    /// - Calculates the remaining volume available for water in gallons,
    ///   considering the substrate height and an additional space left empty at
    ///   the top of the tank.
    ///
    /// # Returns
    /// A new `SubstrateResult` instance populated with calculated volumes and
    /// weights for substrate components and water.
    pub fn new(length: f64, height: f64, width: f64, height_dirt: f64, height_sand: f64) -> Self {
        // convert the area of dirt length * width * height of the dirt (AND
        // WATER) layer from cubic inches to cups, rounded to the nearest half
        let cups_dirt_wet = round_to_nearest_fraction(length * width * height_dirt * CUBIC_INCHES_TO_CUPS, 2);
        
        // estimated volume of dry dirt (80% of volume dirt, 20% water)
        let cups_dirt_dry = round_to_nearest_fraction(cups_dirt_wet * DIRT_TO_WATER_MULTIPLIER, 2);

        // work backwards from total dry volume to get the volumes of the
        // components ignoring the supplement as part of the whole
        let cups_moss = cups_dirt_dry * RATIO_MOSS;
        let cups_soil = cups_dirt_dry * RATIO_SOIL;
        let cups_compost = cups_dirt_dry * RATIO_COMPOST;

        // calculate supplement min/max as total of volume
        let cups_supplement_max = round_to_nearest_fraction(cups_dirt_dry * RATIO_SUPPLEMENT_MAX, 8);
        let cups_supplement_min = round_to_nearest_fraction(cups_dirt_dry * RATIO_SUPPLEMENT_MIN, 8);

        // work backwards from total volume wet, subtracting out the dry
        // components
        let cups_dirt_water = cups_dirt_wet - cups_dirt_dry - cups_supplement_max;

        // Once everything is combined, you will want to scoop so many cups into
        // the fish tank. This calculation provides the approximate number of
        // cups of total dirt mixture you add before water to fill the tank to 1
        // inch
        let cups_dirt_dry_display = cups_dirt_dry + cups_supplement_max;

        // calculates to total volume of sand in cups to equal 2 inches of depth
        // in the tank dimensions
        let cups_sand = round_to_nearest_fraction(height_sand * length * width * CUBIC_INCHES_TO_CUPS, 2);

        // approximate weight of the sand in pounds
        let weight_sand = (cups_sand * CUPS_TO_SAND_WEIGHT_MULTIPLIER).ceil();

        // calculate the remaining area (in gallons) that can be filled with
        // water minus the height of dirt, sand, and some leeway for not
        // overfilling
        let total_substrate_height = height_dirt + height_sand;
        let remaining_water_column = height - total_substrate_height - TOP_OF_TANK_EMPTY_SPACE;
        let water_volume_cubic_inches = length * width * remaining_water_column;
        let water_volume_gallons = round_to_nearest_fraction(water_volume_cubic_inches * CUBIC_INCHES_TO_GALLONS, 4);

        Self {
            height_dirt,
            height_sand,
            cups_dirt_wet,
            cups_dirt_dry_display,
            cups_dirt_water,
            cups_moss,
            cups_soil,
            cups_compost,
            cups_supplement_max,
            cups_supplement_min,
            cups_sand,
            weight_sand,
            water_volume_gallons,
        }
    }

    /// Converts metric measurements to imperial and then initializes a SubstrateResult instance.
    ///
    /// # Arguments
    ///
    /// * `length_cm` - The length of the fish tank in centimeters.
    /// * `width_cm` - The width of the fish tank in centimeters.
    /// * `height_cm` - The height of the fish tank in centimeters.
    /// * `height_dirt_cm` - The height of the dirt layer in centimeters.
    /// * `height_sand_cm` - The height of the sand layer in centimeters.
    ///
    /// # Returns
    ///
    /// A new instance of `SubstrateResult` based on the converted measurements.
    pub fn new_metric(
        length_cm: f64,
        width_cm: f64,
        height_cm: f64,
        height_dirt_cm: f64,
        height_sand_cm: f64,
    ) -> Self {
        // Convert dimensions from centimeters to inches
        let length_in = length_cm / CENTIMETERS_TO_INCHES;
        let width_in = width_cm / CENTIMETERS_TO_INCHES;
        let height_in = height_cm / CENTIMETERS_TO_INCHES;
        let height_dirt_in = height_dirt_cm / CENTIMETERS_TO_INCHES;
        let height_sand_in = height_sand_cm / CENTIMETERS_TO_INCHES;

        // Now call the original new() constructor with converted values
        SubstrateResult::new(length_in, width_in, height_in, height_dirt_in, height_sand_in)
    }

    /// Returns a user-friendly, multi-line formatted summary of the substrate
    /// setup for output.
    pub fn format_summary_imperial(&self, tank_description: String) -> String {
        format!(
            "\
# **{}**
## Dirt Mixture
- Volume of Sphagnum Moss: **{} cups**
- Volume of Unfertilized Soil: **{} cups**
- Volume of Compost: **{} cups**
- Volume of Supplement: **{} - {} cups (min/max)**
## Water
- Volume of Water to Saturate Dirt: **{} cups**
## Added to Tank
- Volume of Dirt Substrate for {} inch layer: **{} cups**
- Volume of Sand for {} inch layer: **{} cups**
## Fill Carefully to Protect Sand Layer
- Volume of Water with 1 inch gap at top of tank: **{} gal**
## Totals
- Volume of Wet Dirt: **{} cups**
- Weight of Sand: **{} pounds**
             ",
            tank_description,
            self.cups_moss,
            self.cups_soil,
            self.cups_compost,
            self.cups_supplement_min,
            self.cups_supplement_max,
            self.cups_dirt_water,
            self.height_dirt,
            self.cups_dirt_dry_display,
            self.height_sand,
            self.cups_sand,
            self.water_volume_gallons,
            self.cups_dirt_wet,
            self.weight_sand,
        )
    }
    
    /// Returns a user-friendly, multi-line formatted summary of the
    /// substrate setup in metric units.
    pub fn format_summary_metric(&self, tank_description: String) -> String {
        format!(            
            "\
# **{}**            
## Dirt Mixture
- Volume of Sphagnum Moss: **{} ml**
- Volume of Unfertilized Soil: **{} ml**
- Volume of Compost: **{} ml**
- Volume of Supplement: **{} - {} ml (min/max)**
## Water
- Volume of Water to Saturate Dirt: **{} ml**
## Added to Tank
- Volume of Dirt Substrate for {} cm layer: **{} ml**
- Volume of Sand for {} cm layer: **{} ml**
## Fill Carefully to Protect Sand Layer
- Volume of Water with 2.5 cm gap at top of tank: **{} litres**
## Totals
- Volume of Wet Dirt: **{} ml**
- Weight of Sand: **{} kg**
             ",
            tank_description,
            round(self.cups_moss * CUPS_TO_ML, 0.1),
            round(self.cups_soil * CUPS_TO_ML, 0.1),
            round(self.cups_compost * CUPS_TO_ML, 0.1),
            round_to_nearest_fraction(self.cups_supplement_min * CUPS_TO_ML, 2),
            round_to_nearest_fraction(self.cups_supplement_max * CUPS_TO_ML, 2),
            round(self.cups_dirt_water * CUPS_TO_ML, 0.1),
            round_to_nearest_fraction(self.height_dirt * CENTIMETERS_TO_INCHES, 2),
            round(self.cups_dirt_dry_display * CUPS_TO_ML, 0.1),
            round_to_nearest_fraction(self.height_sand * CENTIMETERS_TO_INCHES, 2),
            round(self.cups_sand * CUPS_TO_ML, 0.1),
            round_to_nearest_fraction(self.water_volume_gallons * GAL_TO_LITER, 4),
            round(self.cups_dirt_wet * CUPS_TO_ML, 0.1),
            round_to_nearest_fraction(self.weight_sand * LB_TO_KG, 4),
        )
    }
    
}
