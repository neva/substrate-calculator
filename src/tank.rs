#[derive(Debug, Clone)]
pub struct Tank {
    pub description: String,
    pub length: f64,
    pub width: f64,
    pub height: f64,
}

#[derive(Debug, poise::ChoiceParameter)]
pub enum TankImperial {
    #[name = "10 gallon \"Leader\" (20\" x 10\" x 12\")"]
    Tank20x10x12,
    #[name = "15 gallon (24\" x 12\" x 12\")"]
    Tank24x12x12,
    #[name = "15 gallon High (20\" x 10\" x 18\")"]
    Tank20x10x18,
    #[name = "20 gallon High (24\" x 12\" x 16\")"]
    Tank24x12x16,
    #[name = "20 gallon Long (30\" x 12\" x 12\")"]
    Tank30x12x12,
    #[name = "29 gallon (30\" x 12\" x 18\")"]
    Tank30x12x18,
    #[name = "30 gallon Breeder (36\" x 18\" x 12\")"]
    Tank36x18x12,
    #[name = "40 gallon Breeder (36\" x 18\" x 16\")"]
    Tank36x18x16,
    #[name = "40 gallon Long (48\" x 12\" x 16\")"]
    Tank48x12x16,
    #[name = "50 gallon (36\" x 18\" x 19\")"]
    Tank36x18x19,
    #[name = "55 gallon (48\" x 13\" x 21\")"]
    Tank48x13x21,
    #[name = "65 gallon (36\" x 18\" x 24\")"]
    Tank36x18x24,
    #[name = "75 gallon (48\" x 18\" x 21\")"]
    Tank48x18x21,
    #[name = "90 gallon (48\" x 18\" x 24\")"]
    Tank48x18x24,
    #[name = "125 gallon (72\" x 18\" x 21\")"]
    Tank72x18x21,
    #[name = "150 gallon (72\" x 18\" x 28\")"]
    Tank72x18x28,
    #[name = "180 gallon (72\" x 24\" x 25\")"]
    Tank72x24x25,
    #[name = "Custom"]
    TankCustom,
}

impl TankImperial {
    pub fn to_tank(&self) -> Tank {
        match self {
            TankImperial::Tank20x10x12 => Tank {
                description: "10 gallon \"Leader\" (20\" x 10\" x 12\")".to_string(),
                length: 20.0,
                width: 10.0,
                height: 12.0,
            },
            TankImperial::Tank24x12x12 => Tank {
                description: "15 gallon (24\" x 12\" x 12\")".to_string(),
                length: 24.0,
                width: 12.0,
                height: 12.0,
            },
            TankImperial::Tank20x10x18 => Tank {
                description: "15 gallon High (20\" x 10\" x 18\")".to_string(),
                length: 20.0,
                width: 10.0,
                height: 18.0,
            },
            TankImperial::Tank24x12x16 => Tank {
                description: "20 gallon High (24\" x 12\" x 16\")".to_string(),
                length: 24.0,
                width: 12.0,
                height: 16.0,
            },
            TankImperial::Tank30x12x12 => Tank {
                description: "20 gallon Long (30\" x 12\" x 12\")".to_string(),
                length: 30.0,
                width: 12.0,
                height: 12.0,
            },
            TankImperial::Tank30x12x18 => Tank {
                description: "29 gallon (30\" x 12\" x 18\")".to_string(),
                length: 30.0,
                width: 12.0,
                height: 18.0,
            },
            TankImperial::Tank36x18x12 => Tank {
                description: "30 gallon Breeder (36\" x 18\" x 12\")".to_string(),
                length: 36.0,
                width: 18.0,
                height: 12.0,
            },
            TankImperial::Tank36x18x16 => Tank {
                description: "40 gallon Breeder (36\" x 18\" x 16\")".to_string(),
                length: 36.0,
                width: 18.0,
                height: 16.0,
            },
            TankImperial::Tank48x12x16 => Tank {
                description: "40 gallon Long (48\" x 12\" x 16\")".to_string(),
                length: 48.0,
                width: 12.0,
                height: 16.0,
            },
            TankImperial::Tank36x18x19 => Tank {
                description: "50 gallon (36\" x 18\" x 19\")".to_string(),
                length: 36.0,
                width: 18.0,
                height: 19.0,
            },
            TankImperial::Tank48x13x21 => Tank {
                description: "55 gallon (48\" x 13\" x 21\")".to_string(),
                length: 48.0,
                width: 13.0,
                height: 21.0,
            },
            TankImperial::Tank36x18x24 => Tank {
                description: "65 gallon (36\" x 18\" x 24\")".to_string(),
                length: 36.0,
                width: 18.0,
                height: 24.0,
            },
            TankImperial::Tank48x18x21 => Tank {
                description: "75 gallon (48\" x 18\" x 21\")".to_string(),
                length: 48.0,
                width: 18.0,
                height: 21.0,
            },
            TankImperial::Tank48x18x24 => Tank {
                description: "90 gallon (48\" x 18\" x 24\")".to_string(),
                length: 48.0,
                width: 18.0,
                height: 24.0,
            },
            TankImperial::Tank72x18x21 => Tank {
                description: "125 gallon (72\" x 18\" x 21\")".to_string(),
                length: 72.0,
                width: 18.0,
                height: 21.0,
            },
            TankImperial::Tank72x18x28 => Tank {
                description: "150 gallon (72\" x 18\" x 28\")".to_string(),
                length: 72.0,
                width: 18.0,
                height: 28.0,
            },
            TankImperial::Tank72x24x25 => Tank {
                description: "180 gallon (72\" x 24\" x 25\")".to_string(),
                length: 72.0,
                width: 24.0,
                height: 25.0,
            },
            TankImperial::TankCustom => Tank {
                description: "Custom".to_string(),
                length: 0.0,
                width: 0.0,
                height: 0.0,
            }
        }
    }
}

#[derive(Debug, poise::ChoiceParameter)]
pub enum TankMetric {
    #[name = "30-C (30cm x 30cm x 30cm)"]
    Tank30x30x30,
    #[name = "30W (30cm x 18cm x 24cm)"]
    Tank30x18x24,
    #[name = "Mini-M (36cm x 22cm x 26cm)"]
    Tank36x22x26,
    #[name = "45-F (45cm x 24cm x 16cm)"]
    Tank45x24x16,
    #[name = "45-P (45cm x 27cm x 30cm)"]
    Tank45x27x30,
    #[name = "45-H (45cm x 30cm x 45cm)"]
    Tank45x30x45,
    #[name = "45-C (45cm x 45cm x 45cm)"]
    Tank45x45x45,
    #[name = "60-F (60cm x 30cm x 18cm)"]
    Tank60x30x18,
    #[name = "60-P (60cm x 30cm x 36cm)"]
    Tank60x30x36,
    #[name = "60-H (30) (60cm x 30cm x 45cm)"]
    Tank60x30x45_30,
    #[name = "60-H (45) (60cm x 45cm x 45cm)"]
    Tank60x45x45_45,
    #[name = "75-P (75cm x 45cm x 45cm)"]
    Tank75x45x45,
    #[name = "90-P (90cm x 45cm x 45cm)"]
    Tank90x45x45,
    #[name = "90-H (90cm x 45cm x 60cm)"]
    Tank90x45x60,
    #[name = "120-P (120cm x 45cm x 45cm)"]
    Tank120x45x45,
    #[name = "120-H (120cm x 45cm x 60cm)"]
    Tank120x45x60,
    #[name = "180-P (180cm x 45cm x 45cm)"]
    Tank180x45x45,
    #[name = "Custom"]
    TankCustom,
}

impl TankMetric {
    pub fn to_tank(&self) -> Tank {
        match self {
            TankMetric::Tank30x30x30 => Tank {
                description: "30-C (30cm x 30cm x 30cm)".to_string(),
                length: 30.0,
                width: 30.0,
                height: 30.0,
            },
            TankMetric::Tank30x18x24 => Tank {
                description: "30W (30cm x 18cm x 24cm)".to_string(),
                length: 30.0,
                width: 18.0,
                height: 24.0,
            },
            TankMetric::Tank36x22x26 => Tank {
                description: "Mini-M (36cm x 22cm x 26cm)".to_string(),
                length: 36.0,
                width: 22.0,
                height: 26.0,
            },
            TankMetric::Tank45x24x16 => Tank {
                description: "45-F (45cm x 24cm x 16cm)".to_string(),
                length: 45.0,
                width: 24.0,
                height: 16.0,
            },
            TankMetric::Tank45x27x30 => Tank {
                description: "45-P (45cm x 27cm x 30cm)".to_string(),
                length: 45.0,
                width: 27.0,
                height: 30.0,
            },
            TankMetric::Tank45x30x45 => Tank {
                description: "45-H (45cm x 30cm x 45cm)".to_string(),
                length: 45.0,
                width: 30.0,
                height: 45.0,
            },
            TankMetric::Tank45x45x45 => Tank {
                description: "45-C (45cm x 45cm x 45cm)".to_string(),
                length: 45.0,
                width: 45.0,
                height: 45.0,
            },
            TankMetric::Tank60x30x18 => Tank {
                description: "60-F (60cm x 30cm x 18cm)".to_string(),
                length: 60.0,
                width: 30.0,
                height: 18.0,
            },
            TankMetric::Tank60x30x36 => Tank {
                description: "60-P (60cm x 30cm x 36cm)".to_string(),
                length: 60.0,
                width: 30.0,
                height: 36.0,
            },
            TankMetric::Tank60x30x45_30 => Tank {
                description: "60-H (30) (60cm x 30cm x 45cm)".to_string(),
                length: 60.0,
                width: 30.0,
                height: 45.0,
            },
            TankMetric::Tank60x45x45_45 => Tank {
                description: "60-H (45) (60cm x 45cm x 45cm)".to_string(),
                length: 60.0,
                width: 45.0,
                height: 45.0,
            },
            TankMetric::Tank75x45x45 => Tank {
                description: "75-P (75cm x 45cm x 45cm)".to_string(),
                length: 75.0,
                width: 45.0,
                height: 45.0,
            },
            TankMetric::Tank90x45x45 => Tank {
                description: "90-P (90cm x 45cm x 45cm)".to_string(),
                length: 90.0,
                width: 45.0,
                height: 45.0,
            },
            TankMetric::Tank90x45x60 => Tank {
                description: "90-H (90cm x 45cm x 60cm)".to_string(),
                length: 90.0,
                width: 45.0,
                height: 60.0,
            },
            TankMetric::Tank120x45x45 => Tank {
                description: "120-P (120cm x 45cm x 45cm)".to_string(),
                length: 120.0,
                width: 45.0,
                height: 45.0,
            },
            TankMetric::Tank120x45x60 => Tank {
                description: "120-H (120cm x 45cm x 60cm)".to_string(),
                length: 120.0,
                width: 45.0,
                height: 60.0,
            },
            TankMetric::Tank180x45x45 => Tank {
                description: "180-P (180cm x 45cm x 45cm)".to_string(),
                length: 180.0,
                width: 45.0,
                height: 45.0,
            },
            // Example for TankCustom, assuming customization is allowed
            TankMetric::TankCustom => Tank {
                description: "Custom".to_string(),
                length: 0.0, // Placeholder values, assuming they can be set later
                width: 0.0,
                height: 0.0,
            },
        }
    }
}
